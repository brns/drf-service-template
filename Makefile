
# Export the .env file variables to use in scripts
env:
	export $(cat .env | xargs)

ready:
	make format
	make clean
	make safe

format:
	@echo "\n----------------------------"
	@echo Reformat the code
	@echo "----------------------------\n"
	isort -rc .
	black server


clean:
	@echo "\n----------------------------"
	@echo Run tests and checks
	@echo "----------------------------\n"
	pytest
	mypy server
	flake8 server

safe:
	@echo "\n----------------------------"
	@echo Run deployment checks
	@echo "----------------------------\n"
	python manage.py makemigrations --dry-run --check
	python manage.py lintmigrations --exclude-apps django_q oauth2_provider \
          --no-cache

	@echo "Run deployment checks"
	safety check --bare --full-report
	yamllint .gitlab-ci.yml

        DJANGO_ENV=production python manage.py check --deploy --fail-level WARNING


doc:
	doc8 -q docs
