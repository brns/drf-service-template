# https://github.com/agconti/cookiecutter-django-rest/blob/master/%7B%7Bcookiecutter.github_repository_name%7D%7D/wait_for_postgres.py
import logging
import os
from time import sleep
from time import time

import psycopg2

check_timeout = os.getenv("POSTGRES_CHECK_TIMEOUT", 30)
check_interval = os.getenv("POSTGRES_CHECK_INTERVAL", 1)

interval_unit = "second" if check_interval == 1 else "seconds"
config = {
    "dbname": os.getenv("POSTGRES_DB", "django"),
    "user": os.getenv("POSTGRES_USER", "postgres"),
    "password": os.getenv("POSTGRES_PASSWORD", "db_pass"),
    "host": os.getenv("DJANGO_DATABASE_HOST", "127.0.0.1"),
    "port": os.environ.get("DJANGO_DATABASE_PORT", 5433),
}


start_time = time()
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


def pg_isready(host, user, password, dbname, port) -> bool:
    while time() - start_time < float(check_timeout):
        try:
            conn = psycopg2.connect(**vars())
            logger.info("Postgres is ready! ✨ 💅")
            conn.close()
            return True
        except psycopg2.OperationalError:
            logger.info(
                f"Postgres isn't ready. Waiting for {check_interval} {interval_unit}..."
            )
            sleep(float(check_interval))

    logger.error(
        f"We could not connect to Postgres within {check_timeout} seconds."
    )
    return False


pg_isready(**config)
