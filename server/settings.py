"""
Django settings for server project.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their config, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import logging
import os
from pathlib import PurePath
from typing import List
from typing import Tuple

import sentry_sdk
import structlog
from decouple import AutoConfig
from decouple import Csv
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import LoggingIntegration

BASE_DIR = PurePath(__file__).parent.parent

# Optionally override the lookup directory
# for the .env file
DJANGO_ROOT = os.getenv("DJANGO_ROOT", "")

if DJANGO_ROOT:
    BASE_DIR = PurePath(DJANGO_ROOT)


config = AutoConfig(search_path=BASE_DIR)

logging.info(BASE_DIR)
# Managing environment via DJANGO_ENV variable:
os.environ.setdefault("DJANGO_ENV", "development")
DJANGO_ENV = os.environ["DJANGO_ENV"]


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

SECRET_KEY = config("DJANGO_SECRET_KEY")

# Application definition:

INSTALLED_APPS: Tuple[str, ...] = (
    # Apps:
    "server.core",
    # Default django apps:
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # django-admin:
    "django.contrib.admin",
    "django.contrib.admindocs",
    # REST Framework
    "rest_framework",  # utilities for rest apis
    "django_filters",  # for filtering rest endpoints
    "rest_framework.authtoken",
    # Security:
    # https://django-axes.readthedocs.io/en/latest/6_integration.html#integration-with-django-oauth-toolkit
    # "axes", TODO needs setup for oauth and rest
    # Health checks:
    # You may want to enable other checks as well,
    # see: https://github.com/KristianOellegaard/django-health-check
    "health_check",
    "health_check.db",
    "health_check.cache",
    # "health_check.storage",
    # Third party apps
    # "django_http_referrer_policy",
    "django_q",
    "django_extensions",
    "oauth2_provider",
    "corsheaders",
)

MIDDLEWARE: Tuple[str, ...] = (
    # Content Security Policy:
    # "csp.middleware.CSPMiddleware",
    # Django:
    "django.middleware.security.SecurityMiddleware",
    # "django_feature_policy.FeaturePolicyMiddleware",  # django-feature-policy
    "corsheaders.middleware.CorsMiddleware",  # corsheaders
    "oauth2_provider.middleware.OAuth2TokenMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",  # serves static files
    "django.contrib.sessions.middleware.SessionMiddleware",
    #   "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # Axes:
    # "axes.middleware.AxesMiddleware",
    # Django HTTP Referrer Policy:
    # "django_http_referrer_policy.middleware.ReferrerPolicyMiddleware",
)

ROOT_URLCONF = "server.urls"
APPEND_SLASH = True

WSGI_APPLICATION = "server.wsgi.application"


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": config("POSTGRES_DB"),
        "USER": config("POSTGRES_USER"),
        "PASSWORD": config("POSTGRES_PASSWORD"),
        "HOST": config("DJANGO_DATABASE_HOST"),
        "PORT": config("DJANGO_DATABASE_PORT", cast=int, default=5432),
        "CONN_MAX_AGE": config("CONN_MAX_AGE", cast=int, default=60),
        "OPTIONS": {"connect_timeout": 10},
    },
}


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "en-us"

USE_I18N = False
USE_L10N = True

# LANGUAGES = (
#    ("en", ugt("English")),
# )

# LOCALE_PATHS = ("locale/",)

USE_TZ = True
TIME_ZONE = "Europe/Zurich"


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = "/static/"

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)
STATIC_ROOT = BASE_DIR.joinpath("static")
STATICFILES_DIRS: List[str] = []

# Templates
# https://docs.djangoproject.com/en/2.2/ref/templates/api

TEMPLATES = [
    {
        "APP_DIRS": True,
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            # os.path.join(BASE_DIR, "templates")
            # Contains plain text templates, like `robots.txt`:
            # BASE_DIR.joinpath("server", "templates"),
        ],
        "OPTIONS": {
            "context_processors": [
                # Default template context processors:
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                #     "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.request",
            ],
        },
    }
]


# Media files
# Media root dir is commonly changed in production
# (see development.py and production.py).
# https://docs.djangoproject.com/en/2.2/topics/files/

# MEDIA_URL = "/media/"
# MEDIA_ROOT = BASE_DIR.joinpath("media")


# Django authentication system
# https://docs.djangoproject.com/en/2.2/topics/auth/
AUTH_USER_MODEL = "core.User"

AUTHENTICATION_BACKENDS = (
    # "axes.backends.AxesBackend", needs configuration
    "oauth2_provider.backends.OAuth2Backend",
    "django.contrib.auth.backends.ModelBackend",
)

OAUTH2_PROVIDER = {
    # this is the list of available scopes
    "SCOPES": {
        "read": "Read scope",
        "write": "Write scope",
        "groups": "Access to your groups",
        "introspection": "Introspect Tokens",
    },
    "ACCESS_TOKEN_EXPIRE_SECONDS": 86400,  # 1 Day.
}

PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
    "django.contrib.auth.hashers.BCryptPasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.Argon2PasswordHasher",
]


# Security
# https://docs.djangoproject.com/en/2.2/topics/security/

SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_HTTPONLY = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True

X_FRAME_OPTIONS = "DENY"

# https://github.com/DmytroLitvinov/django-http-referrer-policy
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy#Syntax
REFERRER_POLICY = "same-origin"

# https://github.com/adamchainz/django-feature-policy#setting
# FEATURE_POLICY: Dict[str, Union[str, List[str]]] = {}  # noqa: WPS234


# Timeouts
EMAIL_TIMEOUT = 5


# Django Q Task queue
# DjangoORM as broker: less performance but tasks in admin
Q_CLUSTER = {
    "name": "DjangORM",
    "workers": 2,
    "timeout": 90,
    "retry": 120,
    "queue_limit": 50,
    "bulk": 10,
    "orm": "default",
}

##################
# REST_FRAMEWORK #
##################


# Django Rest Framework
REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": config("DJANGO_PAGINATION_LIMIT", default=10, cast=int),
    "DATETIME_FORMAT": "%Y-%m-%dT%H:%M:%S%z",
    "DEFAULT_RENDERER_CLASSES": (
        "rest_framework.renderers.JSONRenderer",
        "rest_framework.renderers.BrowsableAPIRenderer",
    ),
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.IsAuthenticated",
        #  "rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly",
    ],
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "oauth2_provider.contrib.rest_framework.OAuth2Authentication",
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
}


#########
# Cache #
#########

# Caching
# https://docs.djangoproject.com/en/2.2/topics/cache/

CACHES = {
    "default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"},
    # 'axes_cache': {
    #    'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    # },
}


# django-axes
# https://django-axes.readthedocs.io/en/latest/4_configuration.html#configuring-caches

# AXES_CACHE = "axes_cache"

###########
# Logging #
###########

# https://docs.djangoproject.com/en/2.2/topics/logging/

# See also:
# 'Do not log' by Nikita Sobolev (@sobolevn)
# https://sobolevn.me/2020/03/do-not-log


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    # We use these formatters in our `'handlers'` configration.
    # Probably, you won't need to modify these lines.
    # Unless, you know what you are doing.
    "formatters": {
        "json_formatter": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.processors.JSONRenderer(),
        },
        "console": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.processors.KeyValueRenderer(
                key_order=["timestamp", "level", "event", "logger"],
            ),
        },
    },
    # You can easily swap `key/value` (default) output and `json` ones.
    # Use `'json_console'` if you need `json` logs.
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "console"},
        "json_console": {
            "class": "logging.StreamHandler",
            "formatter": "json_formatter",
        },
        # "logfile": {
        #     "level": "DEBUG",
        #     "class": "logging.FileHandler",
        #     "filename": BASE_DIR.joinpath("/../logfile"),
        # },
    },
    # These loggers are required by our app:
    # - django is required when using `logger.getLogger('django')`
    # - security is required by `axes`
    "loggers": {
        "django": {"handlers": ["console"], "propagate": True, "level": "INFO"},
        "security": {
            "handlers": ["console"],
            "level": "ERROR",
            "propagate": False,
        },
    },
    # "root": {"level": "INFO", "handlers": ["console", "logfile"]},
}

structlog.configure(
    processors=[
        structlog.stdlib.filter_by_level,
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.processors.ExceptionPrettyPrinter(),
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ],
    context_class=structlog.threadlocal.wrap_dict(dict),
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)

if DJANGO_ENV == "development":

    """
    This file contains all the settings that defines the development server.

    SECURITY WARNING: don't run with debug turned on in production!
    """

    import logging
    from typing import List

    # Setting the development status:

    DEBUG = True

    ALLOWED_HOSTS = [
        config("DJANGO_HOST", "*"),
        "localhost",
        "0.0.0.0",  # noqa: S104
        "127.0.0.1",
        "[::1]",
    ]

    # Installed apps for developement only:

    INSTALLED_APPS += (
        "debug_toolbar",
        "nplusone.ext.django",
        "django_migration_linter",
    )

    # Django debug toolbar:
    # https://django-debug-toolbar.readthedocs.io
    # Remove whitenoise middleware
    MIDDLEWARE = tuple(
        filter(lambda m: "WhiteNoiseMiddleware" not in m, MIDDLEWARE)
    )

    MIDDLEWARE += (
        "debug_toolbar.middleware.DebugToolbarMiddleware",
        # https://github.com/bradmontgomery/django-querycount
        # Prints how many queries were executed, useful for the APIs.
        "querycount.middleware.QueryCountMiddleware",
    )

    def custom_show_toolbar(request):
        """Only show the debug toolbar to users with the superuser flag.

        :param request: Django Request
        :returns: is superuser
        :rtype: bool

        """
        return request.user.is_superuser

    DEBUG_TOOLBAR_CONFIG = {
        "SHOW_TOOLBAR_CALLBACK": "server.settings.custom_show_toolbar",
    }

    # This will make debug toolbar to work with django-csp,
    # since `ddt` loads some scripts from `ajax.googleapis.com`:
    # CSP_SCRIPT_SRC = ("'self'", "ajax.googleapis.com")
    # CSP_IMG_SRC = ("'self'", "data:")
    # CSP_CONNECT_SRC = ("'self'",)

    # nplusone
    # https://github.com/jmcarp/nplusone

    # Should be the first in line:
    MIDDLEWARE = (
        "nplusone.ext.django.NPlusOneMiddleware",
    ) + MIDDLEWARE  # noqa: WPS440

    # Logging N+1 requests:
    NPLUSONE_RAISE = True  # comment out if you want to allow N+1 requests
    NPLUSONE_LOGGER = logging.getLogger("django")
    NPLUSONE_LOG_LEVEL = logging.WARN
    NPLUSONE_WHITELIST = [
        {"model": "admin.*"},
    ]

    # Force async_tasks to be executed synchronous (no worker required)
    Q_CLUSTER.update({"sync": True})

    # Mail
    EMAIL_HOST = "localhost"
    EMAIL_PORT = 1025
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

    CORS_ORIGIN_ALLOW_ALL = True


elif DJANGO_ENV == "production":
    # Production flags:
    # https://docs.djangoproject.com/en/2.2/howto/deployment/

    DEBUG = False

    ALLOWED_HOSTS = [
        # TODO: check production hosts
        config("DJANGO_HOST"),
        # We need this value for `healthcheck` to work:
        "localhost",
        "0.0.0.0",  # noqa: S104
        "127.0.0.1",
        "[::1]",
    ]

    CORS_ORIGIN_WHITELIST = config("CLIENT_HOSTS", cast=Csv())

    CORS_ORIGIN_REGEX_WHITELIST = config(
        "CLIENT_HOSTS_REGEX", default="", cast=Csv()
    )

    # CSRF_TRUSTED_ORIGINS = CORS_ORIGIN_WHITELIST
    # CORS_ORIGIN_REGEX_WHITELIST = (r"^(https?://)?(\w+\.)?app\.gonnado\.com$"y,)

    # Staticfiles
    # https://docs.djangoproject.com/en/2.2/ref/contrib/staticfiles/

    # This is a hack to allow a special flag to be used with `--dry-run`
    # to test things locally.
    # _COLLECTSTATIC_DRYRUN = config(
    #    'DJANGO_COLLECTSTATIC_DRYRUN', cast=bool, default=False,
    # )
    # Adding STATIC_ROOT to collect static files via 'collectstatic':
    # STATIC_ROOT = ".static" if _COLLECTSTATIC_DRYRUN else "/var/www/django/static"

    # STATICFILES_STORAGE = (
    #     # This is a string, not a tuple,
    #     # but it does not fit into 80 characters rule.
    #     "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"
    # )

    # Media files
    # https://docs.djangoproject.com/en/2.2/topics/files/

    # MEDIA_ROOT = "/var/www/django/media"

    # Password validation
    # https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

    _PASS = "django.contrib.auth.password_validation"  # noqa: S105
    AUTH_PASSWORD_VALIDATORS = [
        {"NAME": "{0}.UserAttributeSimilarityValidator".format(_PASS)},
        {"NAME": "{0}.MinimumLengthValidator".format(_PASS)},
        {"NAME": "{0}.CommonPasswordValidator".format(_PASS)},
        {"NAME": "{0}.NumericPasswordValidator".format(_PASS)},
    ]

    # Security
    # https://docs.djangoproject.com/en/2.2/topics/security/

    # SECURE_HSTS_SECONDS = 31536000  # the same as Caddy has
    # SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    # SECURE_HSTS_PRELOAD = True

    # SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    # SECURE_SSL_REDIRECT = True
    SECURE_REDIRECT_EXEMPT = [
        # This is required for healthcheck to work:
        "^ht/",
    ]

    # SESSION_COOKIE_SECURE = True
    # CSRF_COOKIE_SECURE = True

    #############
    # Reporting #
    #############

    SENTRY_URL = config("SENTRY_URL", default="")

    if SENTRY_URL:
        sentry_logging = LoggingIntegration(
            level=logging.INFO,  # Capture info and above as breadcrumbs
            event_level=logging.WARNING,  # Send warning and above as events
        )

        sentry_sdk.init(  # type: ignore
            dsn=SENTRY_URL,
            integrations=[DjangoIntegration(), sentry_logging],
            send_default_pii=True,
        )

        Q_CLUSTER["error_reporter"] = {"sentry": {"dsn": SENTRY_URL}}


else:
    raise ValueError("Unknown DJANGO_ENV: {0}".format(DJANGO_ENV))


# ALLOWED_HOSTS = ["*"]

CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = ["*"]
