"""business URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

import logging

from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import include
from django.urls import path
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view

from rest_framework_swagger.views import get_swagger_view

# from .views import info
# from .views import trigger_error

urlpatterns = [
    path(r"o/", include("oauth2_provider.urls", namespace="oauth2_provider")),
    path(r"ht/", include("health_check.urls", namespace="health_check")),
    path("", include("server.core.urls")),
]


LOG = logging.getLogger(__name__)

# -----------------------------------------------------------------
# Open API Schema and Documentation
# -----------------------------------------------------------------
# https://www.django-rest-framework.org/api-guide/schemas/
# https://www.django-rest-framework.org/topics/documenting-your-api/
# -----------------------------------------------------------------

schema_view = get_swagger_view(title="REST API Documentation")

urlpatterns += [
    path(
        "openapi/",
        get_schema_view(title="REST API", description="", version="1.0.0",),
        name="openapi-schema",
    ),
    path(
        "swagger/",
        TemplateView.as_view(
            template_name="swagger.html",
            extra_context={"schema_url": "openapi-schema"},
        ),
        name="swagger-ui",
    ),
    path(
        "redoc/",
        TemplateView.as_view(
            template_name="redoc.html",
            extra_context={"schema_url": "openapi-schema"},
        ),
        name="redoc",
    ),
    url(r"^admin/", admin.site.urls),
]

# Media files and storage currently disabled
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path(r"__debug__/", include(debug_toolbar.urls)),
    ]
