from __future__ import annotations

import uuid
from typing import List
from uuid import UUID

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager
from django.contrib.postgres.fields import JSONField
from django.db import models

import model_utils.models

# from django.contrib.auth.models import User as BaseUserManager


# from django.contrib.auth.models import User as BaseUserManager

###############
# Custom User #
###############


class CustomUserManager(UserManager):  # type: ignore
    """Custom Manager for the email user."""

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not email:
            raise ValueError("The given email must be set.")
        email = self.normalize_email(email)

        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()

        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(
            email, password, **extra_fields
        )  # pragma: no cover

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)

    def get_by_natural_key(self, email):
        return self.get_queryset().get(email=email)


class User(AbstractUser):
    """Default Django User with email as ``USERNAME_FIELD``."""

    email = models.EmailField(max_length=255, unique=True, db_index=True)
    """Unique email field that is used as username."""

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS: List[str] = []

    client_data = JSONField(default=dict, null=True)
    """Reserved for a front-end client to store user Meta information."""

    objects: CustomUserManager = CustomUserManager()  # pragma: no cover

    def __str__(self):
        return f"User(email={self.email}, is_staff={self.is_staff}, is_superuser={self.is_superuser})"


#####################
# Abstract Resource #
#####################
# DRAFT for generic
# resource management and
# resource permissions


class ResourceManager(models.Manager):  # type: ignore
    """Base model manager for subclasses of ``AbstractResource``."""

    pass


class AbstractResource(model_utils.models.TimeStampedModel):
    """The base class for a resource."""

    id: models.UUIDField[UUID, UUID] = models.UUIDField(
        primary_key=True, editable=False, default=uuid.uuid4
    )
    """UUID Primary Key"""

    name: models.CharField[str, str] = models.CharField(
        max_length=25, blank=False, null=False
    )
    """Display Name"""

    objects = ResourceManager()

    class Meta:
        abstract = True

    def __str__(self):
        return f"{self.name} (id={self.id})"


###########
# Account #
###########


class AbstractAccount(AbstractResource, model_utils.models.StatusModel):
    STATUS = model_utils.Choices("active", "paused", "archived", "deleted")
    """Current status of the account. Can be one of `active`, `paused`, `archived`, `delete`."""

    class Meta:
        abstract = True
