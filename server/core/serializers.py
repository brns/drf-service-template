from rest_framework import serializers

from .models import User

# from django.db.models import fields


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "url",
            "is_staff",
        )
        read_only_fields = ("id", "email")


class CreateUserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        # call create_user on user object. Without this
        # the password will be stored in plain text.
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "password",
            "first_name",
            "last_name",
            "email",
        )
        read_only_fields = ("is_staff",)
        extra_kwargs = {"password": {"write_only": True}}
