from django.urls import reverse
from rest_framework import status

from .test_views import UserAPITestCase


class OAuthTestCase(UserAPITestCase):
    def test_get_token_and_authorize(self):
        """
        This test case illustrates the process
        to authenticate a user from an insecure frontend
        using password authentication.
        """
        url = reverse("oauth2_provider:token")

        data = {
            "grant_type": "password",
            "username": self.user_data["email"],
            "password": self.user_data["password"],
            "scope": "read",
            "client_id": self.application.client_id,
        }
        response = self.client.post(url, data=data)
        assert response.status_code == status.HTTP_200_OK

        access_token = response.json().get("access_token", "")
        assert access_token

        # Access protected view
        url = reverse("user-list")

        # Ensure the view is protected
        response = self.client.get(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        # Add token to the header
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + access_token)

        response = self.client.get(url + str(self.user.id) + "/")

        assert response.status_code == status.HTTP_200_OK

    def test_create_user_is_protected(self):
        """
        Creating a user is not allowed for tokens
        with only read scope.
        """

        self.create_token_and_authorize("read")

        url = reverse("user-list")

        data = {"username": "odannog", "email": "test@gonnado.com"}

        response = self.client.post(url, data=data)

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_write_scope_allows_creating_users(self):
        """
        Tests that creating users is allowed for
        tokens with write scope.
        """

        self.create_token_and_authorize("read write")

        url = reverse("user-list")

        data = {
            "username": "username",
            "email": "test@gonnado.com",
            "password": "password",
        }

        response = self.client.post(url, data=data)

        assert response.status_code == status.HTTP_201_CREATED
