from django.contrib.auth.hashers import check_password
from django.forms.models import model_to_dict
from django.test import TestCase

import pytest

from ..models import User
from ..serializers import CreateUserSerializer
from .factories import UserFactory


class TestCreateUserSerializer(TestCase):
    def setUp(self):
        self.user_data = model_to_dict(UserFactory.build())

    def test_serializer_with_empty_data(self):
        serializer = CreateUserSerializer(data={})
        assert not serializer.is_valid()

    def test_serializer_with_valid_data(self):
        serializer = CreateUserSerializer(data=self.user_data)
        assert serializer.is_valid()

    def test_serializer_hashes_password(self):
        serializer = CreateUserSerializer(data=self.user_data)
        assert serializer.is_valid()

        user = serializer.save()
        assert check_password(self.user_data.get("password"), user.password)


class TestUserManager(TestCase):
    def setUp(self):
        self.user_data = model_to_dict(UserFactory.build())

    def test_create_superuser(self):

        user = User.objects.create_superuser(
            email=self.user_data["email"], password=self.user_data["password"]
        )

        assert user.is_superuser
        assert user.is_staff

    def test_create_user(self):

        user = User.objects.create_user(email=self.user_data["email"])
        assert user.password  # random password is set

    def test_create_user_ensures_email(self):

        with pytest.raises(ValueError, match="The given email must be set."):
            User.objects.create_user(email="")

    def test_create_superuser_checks_args(self):

        with pytest.raises(
            ValueError, match="Superuser must have is_staff=True."
        ):
            User.objects.create_superuser(email="", is_staff=False)

        with pytest.raises(
            ValueError, match="Superuser must have is_superuser=True."
        ):
            User.objects.create_superuser(email="", is_superuser=False)
