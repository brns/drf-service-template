from typing import Dict
from typing import Union

from django.contrib.auth.hashers import check_password
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

import factory
from faker import Faker
from oauth2_provider.models import Application

from ..models import User
from .factories import ClientApplicationFactory
from .factories import TokenFactory
from .factories import UserFactory

fake = Faker()


class UserAPITestCase(APITestCase):

    user_data: Dict[str, Union[int, str]]
    user: User
    application: Application

    @classmethod
    def setUpClass(cls):
        cls.user_data = factory.build(dict, FACTORY_CLASS=UserFactory)
        cls.application = ClientApplicationFactory.create()

        cls.user = User.objects.create_user(
            cls.user_data["email"],
            password=cls.user_data["password"],
            username=cls.user_data["username"],
            is_staff=True,
            is_superuser=True,
        )

    @classmethod
    def tearDownClass(cls):

        User.objects.all().delete()
        Application.objects.all().delete()

    def create_token_and_authorize(self, scope: str) -> None:
        """Create a token and add it to the request header.

        :param scope: read or write
        """
        access_token = TokenFactory(
            user=self.user, application=self.application, scope=scope
        )

        self.client.credentials(
            HTTP_AUTHORIZATION="Bearer " + access_token.token
        )


class TestUserListTestCase(UserAPITestCase):
    """
    Tests /users list operations.
    """

    def setUp(self):
        self.url = reverse("user-list")

    def test_post_request_with_no_data_fails(self):
        self.create_token_and_authorize("write")
        response = self.client.post(reverse("user-list"), data={})
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_post_request_with_valid_data_succeeds(self):
        self.create_token_and_authorize("write")
        user_data = factory.build(dict, FACTORY_CLASS=UserFactory)
        user_data["username"] = "developer@gonnado.com"

        response = self.client.post(reverse("user-list"), user_data)

        assert response.status_code == status.HTTP_201_CREATED

        user = User.objects.get(pk=response.data.get("id"))  # type: ignore
        user.username == self.user_data.get("username")
        assert check_password(user_data.get("password"), user.password)


class TestUserDetailTestCase(UserAPITestCase):
    """
    Tests /users detail operations.
    """

    def setUp(self):
        self.url = reverse("user-detail", kwargs={"pk": self.user.pk})
        # TODO OAuth
        # self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.user.auth_token}')

    def test_get_request_returns_a_given_user(self):
        self.create_token_and_authorize("read write")
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK

    def test_put_request_updates_a_user(self):
        self.create_token_and_authorize("read write")
        new_first_name = fake.first_name()
        payload = {"first_name": new_first_name}
        response = self.client.put(self.url, payload)

        assert response.status_code == status.HTTP_200_OK

        user = User.objects.get(pk=self.user.pk)
        assert user.first_name == new_first_name


class TestAuthenticatedUserDetails(UserAPITestCase):
    """
    Tests me/ operations.
    """

    def setUp(self) -> None:
        self.url = reverse("me")

    def test_get_user_detail_authenticated(self):
        """Tests GET for the URL with a token and returns success."""

        self.create_token_and_authorize("read")
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK

    def test_get_request_unauthorized(self):
        """Tests GET for the URL without any authentication and returns 401 status."""

        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
