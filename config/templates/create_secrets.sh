#!/bin/bash

kubectl create secret generic drf-secrets \
                --from-literal=POSTGRES_PASSWORD='${POSTGRES_PASSWORD}' \
                --from-literal=DJANGO_SECRET_KEY='${DJANGO_SECRET_KEY}' \
                --from-literal=DJANGO_SUPERUSER_PASSWORD='${DJANGO_SUPERUSER_PASSWORD}'
