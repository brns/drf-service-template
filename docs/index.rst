Welcome to the drf-service documentation!
==================================================

The goal of this project is to provide a template
or core library for a RESTful webservice using `Django <https://www.djangoproject.com/>`_
and the  `Django REST framework <https://www.django-rest-framework.org/>`_ .
It is strongly inspired by the two templates
`wemake-django-template <https://wemake-django-template.readthedocs.io/en/latest/>`_
and `cookiecutter-django-rest <https://github.com/agconti/cookiecutter-django-rest>`_.

Similar to the `wemake-services processes <https://github.com/wemake-services/meta>`_ ,
it aims to focus on code quality, testing,
documentation, security, and scalability.

Note that a big part of this documentation is still a work in progress
and currently taken as is from the
`wemake-django-template <https://wemake-django-template.readthedocs.io/en/latest/>`_.
As not all of the tools are used and some are replaced it may not be
always correct. The corresponding sections
are marked as such.

Goals
-----

When developing this template we had several goals in mind:

- Development environment should be bootstrapped easily,
  so we use ``docker-compose`` for that
- Development should be consistent, so we use strict quality and style checks
- Development, testing, and production should have the same environment,
  so again we develop, test, and run our apps in ``docker`` containers
- Documentation and codebase are the only sources of truth


Limitations
-----------

This project implies that:

- You are using ``docker`` for deployment
- You are using Gitlab and Gitlab CI
- You are not using any frontend assets in ``django``,
  you store your frontend separately


How to start
------------

You should start with reading the documentation.
Reading order is important.

There are multiple processes that you need to get familiar with:

- First time setup phase: what system requirements you must fulfill,
  how to install dependencies, how to start your project
- Active development phase: how to make changes, run tests,


.. toctree::
   :maxdepth: 2
   :caption: Setting things up:

   pages/template/overview.rst
   pages/template/development.rst
   pages/template/django.rst

.. toctree::
   :maxdepth: 2
   :caption: Quality assurance:

   pages/template/documentation.rst
   pages/template/linters.rst
   pages/template/testing.rst
   pages/template/security.rst
   pages/template/gitlab-ci.rst

.. toctree::
   :maxdepth: 2
   :caption: Production:

   pages/template/production-checklist.rst
   pages/template/production.rst
   pages/template/kubernetes.rst

.. toctree::
   :maxdepth: 1
   :caption: Extras:

   pages/template/upgrading-template.rst
   pages/template/faq.rst
   pages/template/troubleshooting.rst

.. toctree::
   :maxdepth: 1
   :caption: Core Models


   pages/project/users.rst
   pages/project/accounts.rst


Indexes and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
