======================
Kubernetes
======================


Introduction
-------------------

`Kubernetes <https://kubernetes.io/>`_ (K8s) is an open-source system for automating deployment,
scaling, and management of containerized applications.

Deployments and related services are specified in so-called Manifest files.
To simplify configuration of those Manifest there exists some package managers.
One of them is the `Helm package manager <https://helm.sh/>`_ that generates
Manifest files from a Chart and a file with customization values.

To not maintain our own Chart we use the well documented `Component Chart <https://devspace.sh/component-chart/docs/introduction>`_.

Configuration
--------------------

The Kubernetes deployment used relies on a config map and a secret
that store and provide all environment variables to the running containers.
No production values have to be stored in the Docker image this way.


The values for the Component Chart are generated
from the template files using `envsubst`
as shown in the following example.

.. code:: bash

   envsubst < django-config.template.yaml | tee manifests/django-config.yaml
   envsubst < values_staging.template.yaml | tee values_staging.yaml


For the Gitab Review Apps all values must be set as `environment variables`_.
Only the `DJANGO_HOST`, `RELEASE_NAME` and `RELEASE_IMAGE`
are set according to pipeline and branch.

.. _`environment variables`: https://docs.gitlab.com/ee/ci/variables/

For the production environment the ConfigMap and Secret must be created
manually in the namespace.

HowTo
------------------

In the following the most important steps to manage and inspect
the deployments are listed. For more informations
please refer to the `official documentation <https://kubernetes.io/docs/setup/>_`.

Connect to the cluster locally
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You need to have `kubectl` installed and
`configurated <https://kubernetes.io/de/docs/tasks/tools/install-kubectl/>`_
in order to communicate with your cluster.
If you have a cluster on digitalocean.com you can follow
`their guide <https://www.digitalocean.com/docs/kubernetes/how-to/connect-to-cluster/>`_.

Initialize production namespace
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The secret and the configmap for the environment variables
have to be created in the new namespace.
Export all production variables in your environment
and execute the following to generate and deploy the
resources.

.. code:: bash

   cd config
   envsubst < templates/config.yaml | tee config.yaml
   kubectl apply -f config.yaml

   kubectl create secret generic drf-secrets \
          --from-literal=POSTGRES_PASSWORD='${POSTGRES_PASSWORD}' \
          --from-literal=DJANGO_SECRET_KEY='${SECRET_DJANGO_KEY}' \
          --from-literal=DJANGO_SUPERUSER_PASSWORD='${DJANGO_SUPERUSER_PASSWORD}'



Set the current namespace
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: bash

   kc get namespaces
   kubectl config set-context --current --namespace <namespace>

The namespace can also be set trough
the `-n/--namespace` option for each command.

Inspect containers
^^^^^^^^^^^^^^^^^^^^^

The `get` command can be used to list the running pods.

.. code:: bash

   kubectl get pods

Access a running container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We can execute arbitratry commands in the containers using `exec`
and
To access the pod using a bash shell we can use it together with

.. code:: bash

   kubectl exec -ti <pod-name> /bin/bash

If there are multiple containers running in the container,
it can be specified using the `-c` option
