Gitlab CI
=========

We use ``Gitlab CI`` to build our containers, test it,
and store them in the internal registry.

These images are then pulled into the production servers.


Configuration
-------------

All configuration is done inside ``.gitlab-ci.yml``.

The test stage should work without further modicifation.
For the review apps and the deployment you need to
add a
`Kubernetes cluster <https://docs.gitlab.com/ee/user/project/clusters/>`_.
to your repository. See :ref:`kubernetes`

For the review apps you need as well to define the
`environment variables <https://docs.gitlab.com/ee/ci/variables/#variables>`_
for the CI pipeline.
Required are the `DJANGO_`, `POSTGRES_` and `CLIENT_` variables
defined in the `.env`.

*Note:* The overall idea follows the
`Auto DevOps <https://docs.gitlab.com/ee/topics/autodevops/>`_
gitlab.com provides. It couldn't be used directly due to
some probles and the latest Kubernetes version at the time.

Pipelines
---------

We have two pipelines configured: for ``master`` and other branches.
On branches we run the tests using a Python image and a database
as `service <https://docs.gitlab.com/ee/ci/services/postgres.html>`_.

Merge requests are deployed as
`review apps <https://docs.gitlab.com/ee/ci/review_apps/>`_.
The exact same Docker
image is used as in the master branch, but with the configuration
values passed to the pipeline.

  .. TODO::
     Restrict this to manual or specific branches.
     Review apps for each branch are mostly not needed.

After successfully deploying the master branch
we also tag the docker image as `latest` and
update this documentation.


Automatic dependencies update
-----------------------------

  .. TODO::
     This should be used, but
     currently it seems to have some issues with poetry
     (https://github.com/dependabot/dependabot-core/issues?q=poetry+is%3Aissue+is%3Aopen+).

You can use `dependabot <https://github.com/dependabot/dependabot-script>`_
to enable automatic dependencies updates via Pull Requests to your repository.
Similar to the original template repository: `list of pull requests <https://github.com/wemake-services/wemake-django-template/pulls?q=is%3Apr+author%3Aapp%2Fdependabot>`_.

It is available to both Github and Gitlab.
But, for Gitlab version you currently have to update your `.gitlab-ci.yml <https://github.com/dependabot/dependabot-script/blob/master/.gitlab-ci.example.yml>`_.


Secret variables
----------------

If some real secret variables are required, then you can use `gitlab secrets <https://docs.gitlab.com/ee/ci/variables/#secret-variables>`_.
And these kind of variables are required *most* of the time.

See :ref:`django` on how to use ``dump-env`` and ``gitlab-ci`` together.


Documentation
-------------
After each deploy from master branch this documentation compiles into nice looking html page.
See `gitlab pages info <https://docs.gitlab.com/ee/user/project/pages/>`_.


Further reading
---------------

- `Container Registry <https://gitlab.com/help/user/project/container_registry>`_
- `Gitlab CI/CD <https://about.gitlab.com/features/gitlab-ci-cd/>`_
