#FROM python:3.7


#FROM tiangolo/uvicorn-gunicorn:python3.7
FROM brns/uvicorn-gunicorn:python3.8

ARG DJANGO_ENV=production

ENV APP_MODULE="server.asgi:application" \
        MODULE_NAME="server.asgi" \
        VARIABLE_NAME="application" \
         # poetry:
        POETRY_VERSION=1.0.5 \
        POETRY_VIRTUALENVS_CREATE=false \
        POETRY_CACHE_DIR='/var/cache/pypoetry' \
        LOG_LEVEL=info

RUN pip install "poetry==$POETRY_VERSION" && poetry --version

# Create project directory (workdir)
#RUN mkdir /app
WORKDIR /app

# Copy only requirements, to cache them in docker layer
COPY ./poetry.lock ./pyproject.toml /app/

# Project initialization:
RUN echo "$DJANGO_ENV" \
        && poetry install \
        $(if [ "$DJANGO_ENV" = 'production' ]; then echo '--no-dev'; fi) \
        --no-interaction --no-ansi \
        # Cleaning poetry installation's cache for production:
        && if [ "$DJANGO_ENV" = 'production' ]; then rm -rf "$POETRY_CACHE_DIR"; fi


# Add requirements.txt to WORKDIR and install dependencies
# COPY requirements.txt .
# RUN pip install -r requirements.txt

# Add the remaining source code files to WORKDIR
COPY . .
#COPY ./config/prestart.sh /app/

# Collect staticfiles (for whitenoise)
RUN python manage.py collectstatic

RUN rm .env
